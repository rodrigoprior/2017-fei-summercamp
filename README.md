# 2017 FEI Summer Camp

This is the material for `2017 Scientific Computing Summer Camp`.

# Directories

- experiment/ - experiment scritps and data

# How to run this legacy environment

In order to run python legacy projects, I suggest that you use [pyenv](https://github.com/pyenv/pyenv) project with [virtualenv](https://github.com/pyenv/pyenv-virtualenv) plugin with the folllowing steps:

```bash
    $ pyenv install 3.6.8  # install python 3.6.8
    $ pyenv virtualenv 3.6.8 fei  # create virtualenv with python 3.6.8
    $ pyenv activate fei
```

# Reference

If you want to use this material please cite this reference:

[Bechelli, R. P. (2016). Content based Image Retrieval Databases Classification with Brain Event Related Potential.](http://www.scitepress.org/Papers/2016/58537/58537.pdf) In Proceedings of the 9th International Joint Conference on Biomedical Engineering Systems and Technologies (DOCTORAL CONSORTIUM 2016) - BIOSIGNALS (pp. 3–8). Roma: SCITEPRESS – Science and Technology Publications, Lda.

    @inproceedings{Bechelli2016,
    abstract = {This paper evaluates and compile information related to Electroencephalography (EEG) used as a pattern to classify a Content Based Image Retrieval (CBIR) system based on an Event Related Potential (ERP) as an input data vector to classify an image database. The Rapid Serial Visual Presentation (RSVP) is used as a method to present multiple images to obtain a series of P300 brain response and specify the duality of target or non- target images (oddball paradigm).},
    address = {Roma},
    author = {Bechelli, R. P.},
    booktitle = {Proceedings of the 9th International Joint Conference on Biomedical Engineering Systems and Technologies (DOCTORAL CONSORTIUM 2016) - BIOSIGNALS},
    pages = {3--8},
    publisher = {SCITEPRESS – Science and Technology Publications, Lda.},
    title = {{Content based Image Retrieval Databases Classification with Brain Event Related Potential}},
    year = {2016}
    pdf = {http://www.scitepress.org/Papers/2016/58537/58537.pdf}
    }

# Images

This experiment use images extracted from [Wikipedia](https://en.wikipedia.org/wiki/Rock%E2%80%93paper%E2%80%93scissors).

# Important links

- http://ipython.org/
- https://www.continuum.io/downloads
- http://pandas.pydata.org/
- https://www.learnpythonthehardway.org/
- http://shop.oreilly.com/product/9781597499576.do
