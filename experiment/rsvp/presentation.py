import csv
import pyglet


class Rsvp(pyglet.window.Window):
    """Generate RSVP (Rapid Serial Visual Representation) test stimulus."""
    def __init__(self, csvpath, imagepath):
        # retrive list os displays to present data in second screen
        display = pyglet.window.get_platform().get_default_display()
        super(Rsvp, self).__init__(fullscreen=False, screen=display.get_screens()[0])
        self.imagepath = imagepath
        self.counter = 0
        self.stimout = []
        with open(csvpath) as f:
            csvdata = csv.reader(f, delimiter=',')
            self.image_list = list(csvdata)

    def on_draw(self):
        """Call images and postion in the center of the screen."""
        self.clear()
        image = pyglet.image.load(
            self.imagepath + self.image_list[self.counter][1])  # get image path
        image.blit(
            (self.get_size()[0] / 2) - 150,
            (self.get_size()[1] / 2) - 150)
        self.stimout = [self.image_list[self.counter][0], self.image_list[self.counter][2]]

    def cron(self):
        """Event dispatcher that redraw screen."""
        self.switch_to()
        self.dispatch_events()
        self.dispatch_event('on_draw')
        self.flip()
        self.counter += 1
