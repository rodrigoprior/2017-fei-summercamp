#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Rapid Serial Visual Presentation (RSVP) build script.

    Usage:
        experiment.py preview
        experiment.py runtest (<subject-id> <session-number>)
        experiment.py -h | --help
        experiment.py --version

    Options:
        -h --help               : Show this screen.
        --version               : Show version.
        preview                 : Preview experiment presentation.
        runtest                 : Run each session for each subject test.

"""
from docopt import docopt
import pandas as pd
from icelera import icelera
from rsvp.presentation import Rsvp
from datetime import datetime
import time

if __name__ == '__main__':
    arguments = docopt(__doc__, version='RSVP 0.0.1')

    if arguments['preview']:
        s = Rsvp(csvpath="./slides/slides.csv", imagepath='')
        s.on_draw()
        s.cron()
        print('Start presentation preview:')
        print("Stimulus information:")
        while True:
            try:
                print("Image id:", s.stimout[0], "Stim id:", s.stimout[1])
                time.sleep(.5)
                s.cron()
            except:
                print("End test")
                break

    if arguments['runtest']:
        # initialize rsvp test
        s = Rsvp(csvpath="./slides/slides.csv", imagepath='')
        s.on_draw()
        s.cron()
        # initialize icelera connection
        ic = icelera.iblue()
        ic.host = '192.168.5.10'
        ic.port = 9000
        ic.group = ['emg']
        data = ic.socket_client()
        # iterate data from iblue
        z = datetime.now()
        data_buffer = []
        i = 0
        print('Receiving data:')
        print('host:', ic.host, 'port:', ic.port, 'group:', ic.group)
        print('Press CTRL+c to stop.')
        try:
            for frame in data:
                data_buffer.append(list(frame)+s.stimout[:])
                i += 1
                if i == 2048:  # 512SPS/0.25Hz to display 4s each image
                    i = 0
                    s.cron()
        except:
            s.close()
            print("Total time:", datetime.now() - z)
            # convert and save data to disk
            fname = "./data/"+arguments['<subject-id>']+"_session"+arguments['<session-number>']+"-signal.csv"
            df_csv = pd.DataFrame(
                data_buffer, columns=ic.channels+['imageid', 'stim'])
            df_csv.to_csv(fname, index=False)
