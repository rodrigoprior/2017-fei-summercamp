import pandas as pd
import numpy as np
from scipy import signal
import matplotlib
import matplotlib.pyplot as plt
from pandas.tools.plotting import radviz, scatter_matrix


def rms(a):
    return np.sqrt(np.mean(np.square(a)))


def filter(x, lowcut, highcut, btype='bandpass', sampling_rate=512, order=4):
    nyq = 0.5 * sampling_rate
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype=btype)
    return signal.filtfilt(b, a, x)


# carrega os dados
df = pd.read_csv('./data/marcosp_session3-signal.csv')
# df = pd.read_csv('./data/victor_session1-signal.csv')
# df = pd.read_csv('./data/victor_session2-signal.csv')
# df = pd.read_csv('./data/victor_session3-signal.csv')

# all signals with no filter
df.plot(subplots=True, title="Signals without filters")
plt.show()

# filtro cut60Hz
df[df.columns.difference(['imageid', 'stim'])] = df[df.columns.difference(['imageid', 'stim'])].apply(lambda x: filter(x, lowcut=58, highcut=62, btype='bandstop'))

# all signals with no with 60Hz bandstop
df.plot(subplots=True, title="Signals with 60Hz bandstop")
plt.show()

# filtro 20Hz to 150Hz
df[df.columns.difference(['imageid', 'stim'])] = df[df.columns.difference(['imageid', 'stim'])].apply(lambda x: filter(x, lowcut=20, highcut=150, btype='bandpass'))

# all signals with filter
df.plot(subplots=True, title="Signals with 60Hz bandstop + 20Hz to 150Hz bandpass")
plt.show()

# apply RMS
delta = df.groupby(['imageid', 'stim']).apply(rms)
delta.drop(['imageid'], axis=1, inplace=True)

# convert voltage to uV
delta[delta.columns.difference(['imageid', 'stim'])] = delta[delta.columns.difference(['imageid', 'stim'])]*1E6

# 0 -> none (black)
# 1 -> rock
# 2 -> paper
# 3 -> scissors


delta.plot(subplots=True)
plt.show()

delta.plot(kind='scatter', x='EX1', y='EX2', c='stim', s=100, cmap='plasma')
plt.show()

scatter_matrix(delta, alpha=0.5, figsize=(10, 6), diagonal='kde')
plt.show()

radviz(delta, class_column='stim')
plt.show()